# react-tty

Docker container for Node-based app development; includes TTY browser interface.

## Ports exposed

* `3000`: For "node server"
* `3001`: The TTY interface
* `35729`: For hot reloads during development

## App paths

The app directory on the server is /app